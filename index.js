const express = require('express');
const app = express();
app.get('/', (req, res) => {
  return res.send('HELLO WORLD');
});
app.listen(8080, () => console.log('server started running on port 8080'));
